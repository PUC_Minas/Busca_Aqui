﻿using BuscaAqui.Controllers.Component.Utils;
using HtmlAgilityPack;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BuscaAqui.Controllers.Component.Crawler {

    /// <summary> 
    /// Classe que faz o devido tratamento e downloads de htmls
    /// </summary>
    public class Fetcher {

        /// <summary>
        /// Carrega seeds do arquivo
        /// </summary>
        /// <returns></returns>
        public static List<string> LoadSeeds() {
            List<string> resp = new List<string>( );
            FileStream seeds = new FileStream( FullPath.PATH_SEEDS, FileMode.Open );
            var line = String.Empty;
            using (StreamReader leitor = new StreamReader( seeds )) {
                while (( line = leitor.ReadLine( ) ) != null) {
                    resp.Add( line );
                }
            }
            return resp;
        }

        /// <summary>
        /// Recupera o html da url de entrada
        /// </summary>
        /// <param name="url"></param>
        /// <returns>Documento html</returns>
        public static HtmlDocument RecoverHtml( string url ) {
            Encoding iso = Encoding.GetEncoding( "iso-8859-1" );
            var web = new HtmlWeb {
                AutoDetectEncoding = false,
                OverrideEncoding = iso
            };
            try {
                return web.Load( url );
            } catch {
                throw;
            }
        }

        /// <summary>
        /// Extrai o titulo da pagina
        /// </summary>
        /// <param name="htmlDocument"></param>
        /// <returns></returns>
        public static string RecoverTitle( HtmlDocument htmlDocument ) {
            var d = htmlDocument.DocumentNode.SelectSingleNode( "//head/title" );
            return TextNormalizer.Normalization( d.InnerText, false );
        }

        /// <summary>
        /// Extrai somente o texto util do html
        /// Remove: espaços extras, acentos, tags htmls e scripts
        /// Passa todo o texto para minusculo
        /// </summary>
        /// <param name="htmlDocument"></param>
        /// <returns></returns>
        public static string RecoverContent( HtmlDocument htmlDocument ) {
            Regex trimmer = new Regex( @"\s\s+|http[^\s]" );
            try {
                htmlDocument.DocumentNode.Descendants( )
                    .Where( n => n.Name == "script" || n.Name == "style" )
                    .ToList( )
                    .ForEach( n => n.Remove( ) );
                StringBuilder textWithoutHtml = new StringBuilder( );
                foreach (HtmlTextNode node in htmlDocument.DocumentNode.SelectNodes( "//text()" )) {
                    textWithoutHtml.AppendLine( node.Text );
                }

                return ( TextNormalizer.Normalization( textWithoutHtml.ToString( ) ) );
            } catch {
                throw;
            }
        }

        

        /// <summary>
        /// Salva a pagina html em disco
        /// </summary>
        /// <param name="html">Html a ser escrito</param>
        /// <param name="nome">Nome para o arquivo</param>
        /// <returns></returns>
        public static string DownloadPage( HtmlDocument html, string nome ) {
            var resp = String.Empty;
            using (var writer = File.CreateText( nome )) {
                writer.Write( html.DocumentNode.InnerHtml );
                resp = html.DocumentNode.InnerHtml;
            }
            return resp;
        }

        /// <summary>
        /// Metodo que recupera todos as urls em uma pagina html
        /// e verifica se a pagina pode ser indexada
        /// </summary>
        /// <param name="html"></param>
        /// <returns>Lista de urls</returns>
        public static List<string> Parser( HtmlDocument html ) {
            List<string> resp = new List<string>( );

            // Verifica se a pagina tem os metas de robots
            var x = html.DocumentNode.SelectNodes( "//meta[@name='robots']" );
            if (x != null) {
                foreach (HtmlNode link in x) {
                    if (link.Attributes[ "content" ].Value.Contains( "noindex" ) ||
                        link.Attributes[ "content" ].Value.Contains( "nofollow" )) {
                        return resp;
                    }
                }
            }

            // Procura por links na pagina
            foreach (HtmlNode link in html.DocumentNode.SelectNodes( "//a[@href]" )) {
                var url = link.GetAttributeValue( "href", string.Empty );
                if (url.Contains( "http" )) {
                    resp.Add( url );
                }
            }
            return resp;
        }

        /// <summary>
        /// Metodo que recupera o arquivo robots
        /// </summary>
        /// <param name="url">Url da pagina</param>
        /// <returns>String com o robots</returns>
        public static string GetRobots( string url ) {
            try {
                return RecoverHtml( "http://" + url + @"/robots.txt" ).DocumentNode.InnerText;
            } catch {
                return String.Empty;
            }

        }
    }
}
