﻿using Com.Bekijkhet.RobotsTxt;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BuscaAqui.Controllers.Component.Crawler {

    /// <summary> 
    /// Classe que trata os robots da pagina
    /// </summary>
    public class RobotsResolver {

        // Todos os agentes
        public static string _userAgent_ = "User-agent: *";

        // Verifica se um caminho e permitido
        public static bool RobotsCanIGoThere( string url, string robot ) {
            Robots robotstxt = new Robots( robot );
            return robotstxt.IsPathAllowed( _userAgent_, url );
        }
    }
}
