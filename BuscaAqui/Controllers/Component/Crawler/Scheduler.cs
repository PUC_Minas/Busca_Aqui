﻿using BuscaAqui.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using System.Collections;
using System.Diagnostics;
using System.Threading;
using System.Text.RegularExpressions;
using System.Text;
using BuscaAqui.Controllers.Component.Utils;

namespace BuscaAqui.Controllers.Component.Crawler {
    /// <summary>
    /// Classe que organiza as requisicoes
    /// </summary>
    public class Scheduler {
        // Urls recuperadas
        public List<Models.Node> URLS { get; set; }
        // Tempo gasto para trabalhar os seeds
        public long SeedsTime { get; set; }
        // Tempo gasto para popular o corpus
        public long PopuleTime { get; set; }
        public List<long> CollectionRate { get; set; }
        public List<string> PendingUrls { get; set; }

        public List<string> UsedURLS { get; set; }

        private Object LockIndex = new Object( );
        private int Index;

        private Object LockUrl = new Object( );

        private int UrlsCount {get;set;}
        private Object lockSum = new Object( );

        /// <summary>
        /// Construtor vazio
        /// </summary>
        public Scheduler() {
            URLS = new List<Models.Node>( );
            CollectionRate = new List<long>( );
            PendingUrls = new List<string>( );
            UsedURLS = new List<string>( );
            SeedsTime = 0L;
            PopuleTime = 0L;
            Index = 0;
        }

        public void Save() {
            using (StreamWriter s = new StreamWriter( FullPath.PATH_CRAWLER_DATA )) {
                s.Write( JsonConvert.SerializeObject(
                    new Scheduler {
                        URLS = URLS,
                        CollectionRate = CollectionRate,
                        PendingUrls = PendingUrls,
                        UsedURLS = UsedURLS,
                        SeedsTime = SeedsTime,
                        PopuleTime = PopuleTime,
                        Index = Index
                    }, Formatting.Indented
                ) );
                s.Close( );
            }
        }

        public Scheduler Load() {
            if (!File.Exists( FullPath.PATH_CRAWLER_DATA )) {
                URLS = new List<Models.Node>( );
                CollectionRate = new List<long>( );
                UsedURLS = new List<string>( );
                PendingUrls = new List<string>( );
                SeedsTime = 0L;
                PopuleTime = 0L;
                Index = 0;
            } else {
                using (StreamReader reader = new StreamReader( FullPath.PATH_CRAWLER_DATA )) {
                    var scheduler = JsonConvert.DeserializeObject<Scheduler>( reader.ReadToEnd( ) );
                    URLS = scheduler.URLS;
                    CollectionRate = scheduler.CollectionRate;
                    PendingUrls = scheduler.PendingUrls;
                    UsedURLS = scheduler.UsedURLS;
                    SeedsTime = scheduler.SeedsTime;
                    PopuleTime = scheduler.PopuleTime;
                    Index = scheduler.Index;
                    reader.Close( );
                }
            }
            return new Scheduler {
                URLS = URLS,
                CollectionRate = CollectionRate,
                PendingUrls = PendingUrls,
                UsedURLS = UsedURLS,
                SeedsTime = SeedsTime,
                PopuleTime = PopuleTime,
                Index = Index
            };
        }
        public int NextIndex() {
            lock (LockIndex) {
                Index++;
                return Index;
            }
        }

        public void IncrementUrlsCount(int newCount) {
            lock (lockSum) {
                UrlsCount = UrlsCount + newCount;
            }
        }

        /// <summary>
        /// Recebe os seeds do usuario ou carrega do arquivo
        /// </summary>
        /// <param name="seedsList">lista de urls para iniciar</param>
        /// <returns>Quantidade de seeds</returns>
        public int Initialize( string seedsList = "" ) {
            var watch = System.Diagnostics.Stopwatch.StartNew( );

            // Tratamento para seeds
            List<string> seeds = new List<string>( );
            if (seedsList.Equals( "" )) {
                seeds = Fetcher.LoadSeeds( );
            } else {
                seeds = new List<string>(
                    seedsList.Split( new string[ ] { "\r\n" },
                    StringSplitOptions.RemoveEmptyEntries ) );
            }

            for (int i = 0; i < seeds.Count( ); i++) {
                var seed = seeds.ElementAt( i );
                var htmlDocument = Fetcher.RecoverHtml( seed );
                var urlsEncontradas = Fetcher.Parser( htmlDocument );
                var documentName = NextIndex( );
                Models.Node r = new Models.Node( ) {
                    Index = documentName,
                    Source = seed,
                    Urls = new Stack<string>( urlsEncontradas )
                };
                URLS.Add( r );

                var text = Fetcher.RecoverContent( htmlDocument );
                DocumentCorpus corpus = new DocumentCorpus( ) {
                    Index = documentName,
                    Url = seed,
                    Content = text,
                    Title = Fetcher.RecoverTitle( htmlDocument ),
                    Source = null
                };
                UsedURLS.Add( seed );

                using (var writer = new StreamWriter( FullPath.PATH_COLLECTION_CORPUS + corpus.Index + ".json" )) {
                    writer.Write( JsonConvert.SerializeObject( corpus, Formatting.Indented ) );
                    writer.Close( );
                }

                Print.WriteLine( ">>> [" + documentName + "] Carregado o Seed: " + seed );
            }

            watch.Stop( );
            SeedsTime = SeedsTime + watch.ElapsedMilliseconds;

            return seeds.Count( );
        }


        /// <summary>
        /// Metodo que popula o corpus com as urls das seeds
        /// e se realimenta com urls encontradas
        /// </summary>
        /// <param name="quantAtual"></param>
        /// <param name="limite"></param>
        public void PopuleLocalCorpus( int quantAtual, int limite = 100 ) {
            var watch = Stopwatch.StartNew( );
            var teste = true;

            Thread vigia = new Thread( () => {
                while (teste) {
                    CollectionRate.Add( Directory.GetFiles( FullPath.PATH_COLLECTION_CORPUS ).Count( ) );
                    Thread.Sleep( 60000 );
                }
            } );
            vigia.Start( );

            var val = quantAtual;
            while (val < limite) {
                int availableThreads = 0;
                int port = 0;
                Print.WriteLine( ">>> Iniciando coleta" );
                try {
                    ThreadPool.GetAvailableThreads( out availableThreads, out port );
                    var max = limite - val < availableThreads / 2 ? limite - val : availableThreads / 2;
                    Print.WriteLine( ">>> Lançando " + max + " threads" );
                    Task[ ] tasks = new Task[ max ];
                    for (int i = 0; i < max; i++) {
                        var temp_i = i;
                        tasks[ temp_i ] = Task.Factory.StartNew( () => Collect( ) );
                    }
                    Task.WaitAll( tasks );
                    Print.WriteLine( ">>> Finalizado coleta de " + max + " documentos, restam " + ( limite - val ) );
                    val = val + max;
                } catch { }
            }
            teste = false;
            Print.WriteLine( ">>> ----------------------------------------" );
            Print.WriteLine( ">>> Fim da coleta, gerando resultados" );
            URLS.ForEach( a => PendingUrls.Add( a.Source ) );
            URLS.ForEach( a => a.Urls.ToList( ).ForEach( b => PendingUrls.Add( b ) ) );
            watch.Stop( );
            PopuleTime = PopuleTime + watch.ElapsedMilliseconds;
        }

        public Tuple<string, int, string> NextNode() {
            lock (LockUrl) {
                Random rnd = new Random( );
                var pos = rnd.Next( 0, URLS.Count( ) );
                string urlAtual = null;
                var index = URLS.ElementAt( pos ).Index;
                var source = URLS.ElementAt( pos ).Source;
                Tuple<string, int, string> resp;
                Models.Node node = URLS.ElementAt( pos );
                while (node.Urls.Count == 0 || ( !node.Urls.Peek( )[ 0 ].Equals( 'h' ) && !node.Urls.Peek( )[ 1 ].Equals( 't' ) && UsedURLS.FirstOrDefault( a => a.Equals( urlAtual ) ) != null )) {
                    if (node.Urls.Count( ) > 0) {
                        UsedURLS.Add( node.Urls.Pop( ) );
                    } else {
                        while (node.Urls.Count( ) == 0) {
                            URLS.RemoveAt( pos );
                            pos = rnd.Next( 0, URLS.Count( ) );
                            node = URLS.ElementAt( pos );
                        }
                    }
                }
                UsedURLS.Add( node.Urls.Peek( ) );
                resp = new Tuple<string, int, string>( node.Urls.Pop( ), node.Index, node.Source );
                return resp;
            }
        }

        public void Collect() {
            var documentName = NextIndex( );
            reTry:
            var node = NextNode( );

            var robotOK = RobotsResolver.RobotsCanIGoThere( node.Item1, Fetcher.GetRobots( new Uri( node.Item1 ).Host ) );
            if (robotOK) {
                try {
                    var htmlDocument = Fetcher.RecoverHtml( node.Item1 );
                    var text = Fetcher.RecoverContent( htmlDocument );
                    DocumentCorpus corpus = new DocumentCorpus( ) {
                        Index = documentName,
                        Url = node.Item1,
                        Content = text,
                        Title = Fetcher.RecoverTitle( htmlDocument ),
                        Source = node.Item3,
                        SourceIndex = node.Item2
                    };

                    using (var writer = new StreamWriter( FullPath.PATH_COLLECTION_CORPUS + corpus.Index + ".json" )) {
                        writer.Write( JsonConvert.SerializeObject( corpus, Formatting.Indented ) );
                        writer.Close( );
                    }

                    var urlsEncontradas = Fetcher.Parser( htmlDocument );
                    if (urlsEncontradas.Count( ) > 0) {
                        Models.Node r = new Models.Node( ) {
                            Index = documentName,
                            Source = node.Item1,
                            Urls = new Stack<string>( urlsEncontradas )
                        };
                        URLS.Add( r );
                        IncrementUrlsCount( r.Urls.Count( ) );
                    }

                    Print.WriteLine( ">>> [" + documentName + "] Coletada a URL: " + node.Item1 );

                } catch (Exception e) {
                    Print.WriteLine( "<<< [" + documentName + "] Falha na URL: " + node.Item1 + " | Erro: " + e.Message );
                    goto reTry;
                }
            } else {
                Print.WriteLine( "<<< [" + documentName + "] Falha na URL: " + node.Item1 + " | Erro: robots não autorizou" );
                goto reTry;
            }
        }

    }
}
