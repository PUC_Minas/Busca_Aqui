﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace BuscaAqui.Controllers.Component.Huffman {
    public class Huffman {

        public static HuffmanTree GenerateTree( string input ) {
            HuffmanTree huffmanTree = new HuffmanTree( );

            // Build the Huffman tree
            huffmanTree.Build( input );
            return huffmanTree;
        }

        public static BitArray Compress( string input, HuffmanTree huffmanTree ) {
            // Encode
            return huffmanTree.Encode( input );
        }

        public static string Descompress( BitArray encoded, HuffmanTree huffmanTree ) {
            // Decode
            return huffmanTree.Decode( encoded );
        }

        public static void Save(string u) {
            using (var writer = File.CreateText( @"C:\Users\p-lea\Desktop\serizp.txt" )) {
                writer.Write( u ); 

            }
        }
    }
}
