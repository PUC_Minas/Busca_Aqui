﻿using BuscaAqui.Controllers.Component.Utils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace BuscaAqui.Controllers.Component.Indexer {
    public class ExternalSort {

        public static long MAX_MEMORY = FullPath.MEMORY_CAN_USE;

        public static List<Tuple<int, int, int>> Run( string path ) {
            FileInfo info = new FileInfo( path );
            var line = "";
            var tuplesList = new List<Tuple<int, int, int>>( );
            Print.WriteLine( ">>> ORDENANDO" );
            if (info.Length < MAX_MEMORY) {
                using (StreamReader file = new System.IO.StreamReader( path )) {
                    while (( line = file.ReadLine( ) ) != null) {
                        //tuplesList.Add( new Tuple<int, int, int>(  Convert.ToInt32(k[0]), Convert.ToInt32( k[1]), Convert.ToInt32( k[2]) ));
                        var k = line.Split( ',' );
                        tuplesList.Add( new Tuple<int, int, int>( Convert.ToInt32( k[ 0 ] ), Convert.ToInt32( k[ 1 ] ), Convert.ToInt32( k[ 2 ] ) ) );
                    }
                    file.Close( );
                }

                tuplesList.Sort( ( t1, t2 ) => {
                    int res = t1.Item1.CompareTo( t2.Item1 );
                    return res != 0 ? res : t2.Item2.CompareTo( t1.Item2 );
                } );
                /*
                using (StreamWriter s = new StreamWriter( path, false )) {
                    tuplesList.ForEach( tupla => s.WriteLine( JsonConvert.SerializeObject( tupla ) ) );
                }*/

                return tuplesList;
            } else {
                var unsortPath = Path.GetDirectoryName( path ) + "unsorted.txt";
                File.Move( path, unsortPath );
                using (FileStream file = new FileStream( path, FileMode.OpenOrCreate, FileAccess.Read )) {
                    file.Close( );
                }
                using (StreamReader file = new System.IO.StreamReader( path )) {
                    using (StreamWriter s = new StreamWriter( path, true )) {
                        while (file.ReadToEnd( ).Length > 0) {
                            Tuple<int, int, int> less = new Tuple<int, int, int>( Int32.MaxValue, Int32.MaxValue, Int32.MaxValue );
                            while (( line = file.ReadLine( ) ) != null) {
                                var actualTuple = JsonConvert.DeserializeObject<Tuple<int, int, int>>( line );
                                if (actualTuple.Item1 <= less.Item1 &&
                                    actualTuple.Item3 <= actualTuple.Item3) {
                                    less = actualTuple;
                                }
                            }
                            s.WriteLine( JsonConvert.SerializeObject( less ) );
                        }
                        s.Close( );
                    }
                    file.Close( );
                }
            }
            return null;
        }

    }
}
