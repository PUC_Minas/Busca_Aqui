﻿using BuscaAqui.Controllers.Component.Utils;
using BuscaAqui.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BuscaAqui.Controllers.Component.Indexer {
    public class Index {

        public long Time { get; private set; }
        public Vocabulary Vocabulary { get; set; }
        public InvertedIndex InvertedIndex { get; set; }
        public int CollectionCount { get; private set; }
        public int AverageDocmenteLength { get; set; }

        public ConcurrentDictionary<int, int> DocumentsLenth { get; set; }

        private Object lockFile = new Object( );
        private Object lockInvertedIndex = new Object( );
        
        private Stopwatch watch;

        public Index(InvertedIndex invertedIndex, Vocabulary vocabulary, long time ) {
            DocumentsLenth = new ConcurrentDictionary<int, int>( );
            InvertedIndex = invertedIndex;
            Vocabulary = vocabulary;
            AverageDocmenteLength = vocabulary.AVG_DocLen;
            Time = time;
        }

        public Index() { }

        /// <summary>
        /// Função que cria tokens para cada uma das palavras de todos os documentos da coleção 
        /// enquanto gera triplas com as ocorrencias das mesmas em cada documento
        /// </summary>
        public void ParserAndTokenizer() {
            watch = Stopwatch.StartNew( );
            Vocabulary vocabulary = Vocabulary.Load();
            List<Tuple<int, int, int>> triple = new List<Tuple<int, int, int>>( );
            LoadDocLength( );

            // Define o tamanho da coleção atual
            CollectionCount = Directory.GetFiles( FullPath.PATH_COLLECTION_CORPUS ).Count();
            InvertedIndex.CollectionSize = CollectionCount;
            InvertedIndex.Save( );

            // Para continuar contando multiplica pelo tamanho da coleção
            if (AverageDocmenteLength != 0) {
                AverageDocmenteLength = AverageDocmenteLength * CollectionCount;
            }

            var collection = Directory.GetFiles( FullPath.PATH_COLLECTION_CORPUS, "*.json" );
            if (collection.Count() == 0) {
                Compression.DecompressDirectory( FullPath.PATH_COLLECTION_CORPUS );
                collection = Directory.GetFiles( FullPath.PATH_COLLECTION_CORPUS, "*.json" );
            }
            Task[ ] tasks = new Task[ collection.Length ];
            for (int i = 0; i < collection.Length; i++) {
                var temp_i = i;
                tasks[ i ] = new Task( () => Tokenizer( vocabulary, collection[ temp_i ] ) );
                tasks[ i ].Start( );
                tasks[ i ].Wait( );
                Compression.CompressFile( new FileInfo( collection[ temp_i ] ) );
                SaveDocLength( );
                double teste = i % 1;
                if(teste == 0) {
                    vocabulary.Save( );
                    Vocabulary = vocabulary;
                    Analyser( );
                }
                while (FullPath.INUSE == true) {
                    Print.WriteLine( ">>> Aguardando finalaziação da query" );
                    Thread.Sleep( 30000 );
                }
            }       
            Print.WriteLine( ">>> Finalizado indexação de todos os documentos" );

            // Divide para obter a média
            AverageDocmenteLength = AverageDocmenteLength / CollectionCount;
            vocabulary.AVG_DocLen = AverageDocmenteLength;
            vocabulary.Save( );
        }
        
        private void Tokenizer(Vocabulary vocabulary, string file) {
            var temporaryTripleList = new StringBuilder( );
            var tripleList = "";
            var text = File.ReadAllText( file );
            var data = JsonConvert.DeserializeObject<DocumentCorpus>( text );
            var words = data.Content.Split( new char[ ] { ' ', ',' } );
            AverageDocmenteLength = AverageDocmenteLength + words.Count( );

            if (words.Count( ) > 0) {
                for (int i = 0; i < words.Length; i++) {
                    var word = words[ i ];
                    if (!word.Equals( "" )) {
                        var token = vocabulary.Search( word );
                        if (token == 0) {
                            token = vocabulary.Insert( word );
                        }
                        var occurence = new Tuple<int, int, int>( token, data.Index, i );
                        Print.WriteLine( ">>> [" + data.Index + "] coletada a palavra - " + word );
                        tripleList = tripleList + occurence.Item1 + "," + occurence.Item2 + "," + occurence.Item3 + "\n";
                    }
                }
            }
            Print.WriteLine( ">>> FIM DO DOCUMENTO [" + data.Index + "] " );

            File.Create( FullPath.PATH_LOG + data.Index );
            
            DocumentsLenth.TryAdd( data.Index, words.Length );
            temporaryTripleList.Append( tripleList );

            AppendFile( temporaryTripleList );
            temporaryTripleList.Clear( );
        }

        /// <summary>
        /// Função que gera o indice invertido a partir das triplas em disco
        /// </summary>
        public void Analyser() {
            InvertedIndex invertedIndex = InvertedIndex.Load();
            FileInfo info = new FileInfo( FullPath.PATH_TEMPORARY_TRIPLES );
            if (info.Length > 0) {
                Print.WriteLine( ">>> Iniciado ordenação externa" );
                var orderedTriples = ExternalSort.Run( FullPath.PATH_TEMPORARY_TRIPLES );
                var lastToken = orderedTriples.First( ).Item1;
                OccurrenceList occurenceList = new OccurrenceList( );
                List<Task> tasks = new List<Task>( );

                var triples = new List<Tuple<int, int, int>>( );
                foreach (var triple in orderedTriples) {
                    if (triple.Item1 == lastToken) {
                        triples.Add( triple );
                    } else {
                        var temp_token = lastToken;
                        var temp_triples = JsonConvert.SerializeObject( triples );
                        var temp_triplesFinal = JsonConvert.DeserializeObject<List<Tuple<int, int, int>>>( temp_triples );
                        tasks.Add( Task.Factory.StartNew( () => CreateListOccorrence( temp_triplesFinal, temp_token ) ) );
                        triples = new List<Tuple<int, int, int>>( );
                        triples.Add( triple );

                        lastToken = triple.Item1;
                    }
                }
                Task.WaitAll( tasks.ToArray( ) );


                invertedIndex = InvertedIndex;
                invertedIndex.CollectionSize = Directory.GetFiles( FullPath.PATH_COLLECTION_CORPUS ).Count( );
                watch.Stop( );
                Time = watch.ElapsedMilliseconds;
                invertedIndex.Time = invertedIndex.Time + Time;
                invertedIndex.Save( );

                InvertedIndex = invertedIndex;
            }
            // Deleta as triplas
            File.Delete( FullPath.PATH_TEMPORARY_TRIPLES );
        }

        public void CreateListOccorrence( List<Tuple<int,int,int>> orderedTriples, int token) {
            var lastDocument = orderedTriples.First().Item2;
            var lastToken = orderedTriples.First( ).Item1;
            
            Occurrence occurence = new Occurrence(lastDocument, new List<int> ());
            OccurrenceList occurenceList = new OccurrenceList( );
            foreach (var triple in orderedTriples) {
                if (triple.Item2 == lastDocument) {
                    occurence.WordPositions.Add( triple.Item3 );
                } else {

                    occurenceList.List.Add( occurence );
                    occurence = new Occurrence( triple.Item2, new List<int>( ) );
                    occurence.WordPositions.Add( triple.Item3 );
                    lastDocument = triple.Item2;
                }
            }
            occurenceList.List.Add( occurence );
            Print.WriteLine( ">>> Criado lista de ocorrencias do Token "+lastToken );
            
            InvertedIndex.Add( lastToken, occurenceList );
        }

        private void AppendFile(StringBuilder temporaryTripleList) {
            lock (lockFile) {
                using (StreamWriter s = new StreamWriter( FullPath.PATH_TEMPORARY_TRIPLES, true )) {
                    s.Write( temporaryTripleList.ToString( ) );
                    s.Close( );
                }
            }
        }

        public void SaveDocLength() {            
            using (StreamWriter writer = new StreamWriter( FullPath.PATH_RANK_DOC_LEN )) {
                var temp = DocumentsLenth.ToDictionary( ( KeyValuePair<int, int> kvp ) => kvp.Key, 
                                                        ( KeyValuePair<int, int> kvp ) => kvp.Value );
                writer.WriteLine( JsonConvert.SerializeObject( temp, Formatting.Indented ) );
                writer.Close( );
            }
        }

        public void LoadDocLength() {
            if (File.Exists( FullPath.PATH_RANK_DOC_LEN )){
                using (StreamReader reader = new StreamReader( FullPath.PATH_RANK_DOC_LEN )) {
                    var temp = JsonConvert.DeserializeObject<Dictionary<int, int>>( reader.ReadToEnd( ) );
                    reader.Close( );
                    DocumentsLenth = new ConcurrentDictionary<int, int>( temp );
                }
            } else {
                DocumentsLenth = new ConcurrentDictionary<int, int>( );
            }
        }

    }
}
