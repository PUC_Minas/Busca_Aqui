﻿using BuscaAqui.Controllers.Component.Utils;
using BuscaAqui.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuscaAqui.Controllers.Component.Query {
    public class Rank {
        double k1 = 1;
        double b = 0.75;
        public Rank() { }

        public List<Tuple<string, string, string>> BM25( string query ) {
            InvertedIndex invertedIndex = new InvertedIndex( );
            invertedIndex.Load( );
            Vocabulary vocabulary = new Vocabulary( );
            vocabulary.Load( );
            var queryWords = query.Split( ' ' );

            Dictionary<int, int> documentsLen = null; ;
            using(StreamReader reader = new StreamReader( FullPath.PATH_RANK_DOC_LEN )) {
                var t = reader.ReadToEnd( );
                documentsLen = JsonConvert.DeserializeObject<Dictionary<int, int>>( t );
                reader.Close( );
            }

            Dictionary<int, double> resp = new Dictionary<int, double>( );

            foreach (var x in queryWords) {
                double temp = 0;
                var tokenId = vocabulary.Search( x );
                if (tokenId != 0) {
                    var occList = invertedIndex.Search( tokenId );
                    foreach (var y in occList.List) {
                        temp = ( k1 + y.WordPositions.Count( ) / k1 * ( ( 1 - b ) + b * documentsLen[y.DocumentID] / vocabulary.AVG_DocLen ) + y.WordPositions.Count( ) ) * occList.Idf;
                        //temp = ( k1 + y.WordPositions.Count( ) / k1 * ( ( 1 - b ) + b * occList.List.Count( ) / vocabulary.AVG_DocLen ) + y.WordPositions.Count( ) ) * occList.Idf;
                        var inserted = resp.TryAdd( y.DocumentID, temp );
                        if (!inserted) {
                            resp.TryGetValue( y.DocumentID, out double number );
                            resp[ y.DocumentID ] = number + temp;
                        }
                    }
                }
            }
            var respOredered = resp.OrderByDescending( a => a.Value ).ToList( );
            var snippets = new List<Tuple<string, string, string>>( );
            StringBuilder rankLog = new StringBuilder( );
            foreach (var x in respOredered) {
                Compression.DecompressFile( new FileInfo( FullPath.PATH_COLLECTION_CORPUS + @"\" + x.Key + ".json.gz" ) );
                DocumentCorpus document = DocumentCorpus.Load(x.Key);
                StringBuilder snippetFinal = new StringBuilder();
                string snippet = "";

                var text = document.Content.Split( ' ' );
                foreach (var word in queryWords) {
                    var tokenId = vocabulary.Search( word );
                    if (tokenId != 0) {
                        var occ = invertedIndex.Search( tokenId ).List.SingleOrDefault( a => a.DocumentID == x.Key );
                        if (occ != null) {
                            snippetFinal.Append( " ... " );
                            var pos = occ.WordPositions.First( );
                            for (int i = 1; i <= 40 / queryWords.Length; i++) {
                                snippet =  ( pos - i <= 0 ? "" : text[ pos - i ] ) + " " + snippet + " ";
                            }
                            snippet = snippet  + " <b>" +  text[ pos ] + "</b> ";
                            for (int i = 1; i <= 40 / queryWords.Length; i++) {
                                snippet = snippet +  ( pos + i >= text.Length ? "" : text[ pos + i ] ) + " ";
                            }
                            snippetFinal.Append( snippet );
                            snippet = "";
                        }                    
                    }
                }

                //rankLog.Append( "501 0 " + ( document.ExternalIndex.Equals( "" ) ? document.Index.ToString( ) : document.ExternalIndex ) + " 0 \n" );
                snippets.Add( new Tuple<string, string, string>( document.Title.Length > 0 ? document.Title : "Documento sem titulo", snippetFinal.ToString() + " ... ", document.Url ) );
                snippetFinal = new StringBuilder( );
                snippet = String.Empty;
                Compression.CompressFile( new FileInfo( FullPath.PATH_COLLECTION_CORPUS + @"\" + x.Key + ".json" ) );
            }

            using(StreamWriter writer = new StreamWriter( FullPath.PATH_RANK_LOG )) {
                writer.Write( rankLog.ToString( ) );
                writer.Close( );
            }

            return snippets;
        }
    }
}
