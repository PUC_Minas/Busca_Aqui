﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading.Tasks;

namespace BuscaAqui.Controllers.Component.Utils {
    public class Compression {

        public static void CompressDirectory( string path ) {
            DirectoryInfo infoDir = new DirectoryInfo( path );
            foreach (FileInfo fileToCompress in infoDir.GetFiles( "*.json" )) {
                CompressFile( fileToCompress );
            }
        }

        public static void CompressFile( FileInfo fileToCompress, bool deleteOriginal = true ) {
            using (FileStream originalFileStream = fileToCompress.OpenRead( )) {
                if (( System.IO.File.GetAttributes( fileToCompress.FullName ) &
                   FileAttributes.Hidden ) != FileAttributes.Hidden & fileToCompress.Extension != ".gz") {
                    using (FileStream compressedFileStream = System.IO.File.Create( fileToCompress.FullName + ".gz" )) {
                        using (GZipStream compressionStream = new GZipStream( compressedFileStream,
                           CompressionMode.Compress )) {
                            originalFileStream.CopyTo( compressionStream );
                        }
                    }
                }
            }
            if (deleteOriginal) {
                fileToCompress.Delete( );
            }
        }

        public static void DecompressDirectory( string path ) {
            DirectoryInfo directorySelected = new DirectoryInfo( path );
            foreach (FileInfo fileToDecompress in directorySelected.GetFiles( "*.gz" )) {
                DecompressFile( fileToDecompress );
            }
        }

        public static string DecompressFile( FileInfo fileToDecompress, bool deleteCompress = true ) {
            string newFileName = "";
            using (FileStream originalFileStream = fileToDecompress.OpenRead( )) {
                string currentFileName = fileToDecompress.FullName;
                newFileName = currentFileName.Remove( currentFileName.Length - fileToDecompress.Extension.Length );

                using (FileStream decompressedFileStream = System.IO.File.Create( newFileName )) {
                    using (GZipStream decompressionStream = new GZipStream( originalFileStream, CompressionMode.Decompress )) {
                        decompressionStream.CopyTo( decompressedFileStream );
                    }
                }
            }
            if (deleteCompress) {
                fileToDecompress.Delete( );
            }
            return newFileName;
        }

    }
}
