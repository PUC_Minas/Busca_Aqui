﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BuscaAqui.Controllers.Component.Utils {
    public class ConvertTime {
        public static string Convert( long time ) {
            if (time > 10000000) {
                return ( time / 10000000 ) + "h";
            } else if (time > 100000) {
                return ( time / 100000 ) + "m";
            } else if (time > 1000) {
                return ( time / 1000 ) + "s";
            }
            return time + "ms";
        }
    }
}
