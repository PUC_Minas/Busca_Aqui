﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BuscaAqui.Controllers.Component.Utils {
    public class FullPath {
        public static bool INUSE = false;

        public static long MEMORY_CAN_USE = 4294967296; // In bytes

        public static string PATH_INDEX = AppContext.BaseDirectory + @"\Index\";

        public static string PATH_INVERTED_INDEX = AppContext.BaseDirectory + @"\Index\InvertedIndex.json";

        public static string PATH_TEMPORARY_TRIPLES = AppContext.BaseDirectory + @"\Index\TemporaryTriples.txt";

        public static string PATH_COLLECTION_CORPUS = AppContext.BaseDirectory + @"\Corpus\";

        public static string PATH_VOCABULARY = AppContext.BaseDirectory + @"\Index\Vocabulary.json";

        public static string PATH_CRAWLER_DATA = AppContext.BaseDirectory + @"\Crawler\Crawler.json";

        public static string PATH_CRAWLER= AppContext.BaseDirectory + @"\Crawler\";

        public static string PATH_SEEDS = AppContext.BaseDirectory + @"\Seeds.txt";

        public static string PATH_HUFFMAN = AppContext.BaseDirectory + @"\Huffman.json";

        public static string PATH_QUERY = AppContext.BaseDirectory + @"\Query\";

        public static string PATH_QUERY_LOG = AppContext.BaseDirectory + @"\Query\QueryLog.json";

        public static string PATH_RANK_DOC_LEN = AppContext.BaseDirectory + @"\Query\DocumentsLen.json";

        public static string PATH_LOG = AppContext.BaseDirectory + @"\LOG\";

        public static string PATH_RANK_LOG = AppContext.BaseDirectory + @"\LOG\RankLog.txt";
    }
}

