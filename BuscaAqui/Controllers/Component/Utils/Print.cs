﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace BuscaAqui.Controllers.Component.Utils {
    public class Print {
        public static void WriteLine(string text) {
            Console.WriteLine( text );
            Debug.WriteLine( text );
        }

        public static void Write( string text ) {
            Console.Write( text );
            Debug.Write( text );
        }
    }
}
