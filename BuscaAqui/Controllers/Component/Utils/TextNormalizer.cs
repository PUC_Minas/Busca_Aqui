﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BuscaAqui.Controllers.Component.Utils {
    public class TextNormalizer {

        /// <summary>
        /// Normaliza o texto, removendo espaçamentos em excesso, acentos e caracteres especiais
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string Normalization( string text, bool total = true ) {
            if (total) {
                text = text.ToLower( );
            }
            string[ ] split = text.Split( new char[ ] { ' ', '\t', '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries );
            var textWithoutExtraWhitespace = string.Join( " ", split );

            var normalizedText = "";
            foreach (var x in textWithoutExtraWhitespace) {
                if (x == 'ã' || x == 'á' || x == 'ã' || x == 'à' || x == 'â' || x == 'ā' || x == 'ä' || x == 'å' || x == 'ą' || x == 'ă') {
                    normalizedText = normalizedText + 'a';
                } else if (x == 'ç' || x == 'ć' || x == 'č') {
                    normalizedText = normalizedText + 'c';
                } else if (x == 'ď' || x == 'đ' || x == 'ð') {
                    normalizedText = normalizedText + 'd';
                } else if (x == 'é' || x == 'ê' || x == 'è' || x == 'ę' || x == 'ë' || x == 'ė' || x == 'ē' || x == 'ě') {
                    normalizedText = normalizedText + 'e';
                } else if (x == 'ķ') {
                    normalizedText = normalizedText + 'k';
                } else if (x == 'ľ' || x == 'ĺ' || x == 'ļ' || x == 'ł') {
                    normalizedText = normalizedText + 'l';
                } else if (x == 'ñ' || x == 'ń' || x == 'ņ' || x == 'ň') {
                    normalizedText = normalizedText + 'n';
                } else if (x == 'ó' || x == 'ô' || x == 'õ' || x == 'ò' || x == 'ø' || x == 'ø' || x == 'ő') {
                    normalizedText = normalizedText + 'o';
                } else if (x == 'ř' || x == 'ŕ' || x == 'ŗ') {
                    normalizedText = normalizedText + 'r';
                } else if (x == 'ś' || x == 'š' || x == 'ş' || x == 'ş' || x == 'ș') {
                    normalizedText = normalizedText + 's';
                } else if (x == 'ť' || x == 'ț' || x == 'ţ') {
                    normalizedText = normalizedText + 't';
                } else if (x == 'ú' || x == 'ü' || x == 'ù' || x == 'û' || x == 'ū' || x == 'ů' || x == 'ű' || x == 'ų') {
                    normalizedText = normalizedText + 'u';
                } else if (x == 'ç' || x == 'ć' || x == 'č') {
                    normalizedText = normalizedText + 'c';
                } else if (x == 'ÿ' || x == 'ý' || x == 'ỳ' || x == 'ỷ' || x == 'ỹ' || x == 'ý' || x == 'ŷ') {
                    normalizedText = normalizedText + 'y';
                } else if (x == 'ź' || x == 'ž' || x == 'ż') {
                    normalizedText = normalizedText + 'z';
                } else if (x == '-') {
                    normalizedText = normalizedText + '-';
                } else if (char.IsWhiteSpace( x )) {
                    normalizedText = normalizedText + ' ';
                } else if (!char.IsLetterOrDigit( x ) && total) {
                    normalizedText = normalizedText + "";
                } else {
                    normalizedText = normalizedText + x;
                }
            }

            return normalizedText;
        }

    }
}
