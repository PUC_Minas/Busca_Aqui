﻿using BuscaAqui.Controllers.Component.Crawler;
using BuscaAqui.Models;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BuscaAqui.Controllers.Component.Utils {
    public class WT10G {
        public WT10G() {

        }

        public void GenerateCorpus( string path ) {
            var index = 0;
            string[ ] directories = Directory.GetDirectories( path );
            foreach (var dir in directories) {
                //Compression.DecompressDirectory( dir );
                foreach (var file in Directory.GetFiles( dir )) {
                    string fileName = Compression.DecompressFile( new FileInfo(file) );
                    var text = "";
                    using (StreamReader reader = new StreamReader( fileName )) {
                        text = reader.ReadToEnd( );
                        reader.Close( );
                    }
                    var words = text.Split( new char[ 0 ], StringSplitOptions.RemoveEmptyEntries );
                    DocumentCorpus document = new DocumentCorpus( );
                    var contentIgnore = true;
                    //var titleIgnore = true;
                    StringBuilder content = new StringBuilder( );
                    for (int i = 0; i < words.Count( ); i++) {
                        if (words[ i ].Contains( "<DOCNO>" )) {
                            document.ExternalIndex = words[ i ].Replace( "<DOCNO>", "" ).Replace( "</DOCNO>", "" );
                        } else if (words[ i ].Contains( "<DOCHDR>" )) {
                            document.Url = words[ i + 1 ];
                        } else if (words[ i ].Contains( "</DOCHDR>" )) {
                            contentIgnore = false;
                        } else if (( words[ i ].ToLower( ).Contains( "<title>" ) && words[ i ].ToLower( ).Contains( "</title>" ) )) {
                            document.Title = words[ i ].ToLower( ).Replace( "<title>", "" ).Replace( "</title>", "" );
                        /*} else if (( words[ i ].ToLower( ).Contains( "<title>" ) )) {
                            document.Title = document.Title + " " + words[ i ].Replace( "<title>", "" ).Replace( "</title>", "" );
                            titleIgnore = false;
                        } else if (( words[ i ].ToLower( ).Contains( "</title>" ) )) {
                            document.Title = document.Title + " " + words[ i ].Replace( "<title>", "" ).Replace( "</title>", "" );
                            titleIgnore = true;
                        */} else if (( words[ i ].Equals( "<DOC>" ) || words[ i ].Equals( "</DOC>" ) ) && i != 0 && content.Length > 0) {
                            string noHTML = Regex.Replace( content.ToString( ), @"<[^>]+>|&nbsp;", "" ).Trim( );
                            document.Content = TextNormalizer.Normalization( noHTML );
                            index++;
                            document.Index = index;
                            //if (document.Title == null || document.Title.Equals( "" )) {
                            document.Title = document.ExternalIndex;
                            //}
                            DocumentCorpus.Save( document );
                            Compression.CompressFile( new FileInfo( FullPath.PATH_COLLECTION_CORPUS + document.Index + ".json" ) );
                            Print.WriteLine( ">>> WT10G documento [" + document.ExternalIndex + "] salvo com indice [" + document.Index + "]" );
                            contentIgnore = true;
                            //titleIgnore = true;
                            content.Clear( );
                        /*} else if (!titleIgnore) {
                            document.Title = document.Title + " " + words[ i ].Replace( "<title>", "" ).Replace( "</title>", "" );
                        */} else if (!contentIgnore) {
                            content.Append( words[ i ] + " " );
                        }
                    }
                    //File.Delete( file );
                    Compression.CompressFile( new FileInfo( fileName ) );
                }
            }
           // Compression.CompressDirectory( FullPath.PATH_COLLECTION_CORPUS );
        }
    }
}
