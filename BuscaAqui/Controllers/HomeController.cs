﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using BuscaAqui.Controllers.Component;
using BuscaAqui.Controllers.Component.Crawler;
using System.IO;
using BuscaAqui.Models;
using BuscaAqui.Controllers.Component.Indexer;
using System.IO.Compression;
using BuscaAqui.Controllers.Component.Query;
using Gma.DataStructures.StringSearch;
using Newtonsoft.Json;
using System.Diagnostics;
using BuscaAqui.Controllers.Component.Utils;
using System.Threading.Tasks;

namespace BuscaAqui.Controllers {

    public class HomeController : Controller {

        public IActionResult Index() {
            if (!Directory.Exists( FullPath.PATH_COLLECTION_CORPUS )) {
                Directory.CreateDirectory( FullPath.PATH_COLLECTION_CORPUS );
            }
            if (!Directory.Exists( FullPath.PATH_CRAWLER )) {
                Directory.CreateDirectory( FullPath.PATH_CRAWLER );
            }
            if (!Directory.Exists( FullPath.PATH_INDEX )) {
                Directory.CreateDirectory( FullPath.PATH_INDEX );
            }
            if (!Directory.Exists( FullPath.PATH_QUERY )) {
                Directory.CreateDirectory( FullPath.PATH_QUERY );
            }
            if (!Directory.Exists( FullPath.PATH_LOG )) {
                Directory.CreateDirectory( FullPath.PATH_LOG );
            }

            var query = Request.Query[ "query" ].ToString( );
            if (!Request.QueryString.Value.Equals( "" )) {

                FullPath.INUSE = true;

                List<string> queryLog = new List<string>( );
                if (System.IO.File.Exists( FullPath.PATH_QUERY_LOG )) {
                    using (StreamReader reader = new StreamReader( FullPath.PATH_QUERY_LOG )) {
                        var text = reader.ReadToEnd( );
                        queryLog = JsonConvert.DeserializeObject<List<string>>( text );
                        reader.Close( );
                    }
                }
                if (queryLog.SingleOrDefault( a => a.ToLower( ).Equals( query.ToLower( ) ) ) == null && !query.Equals( " " ) && !query.Equals( "" )) {
                    if (query[ 0 ] == ' ') {
                        query.Remove( 0 );
                    }
                    if (query[ query.Length - 1 ] == ' ') {
                        query.Remove( query.Length - 1 );
                    }
                    queryLog.Add( query );
                }
                using (StreamWriter writer = new StreamWriter( FullPath.PATH_QUERY_LOG )) {
                    writer.Write( JsonConvert.SerializeObject( queryLog, Formatting.Indented ) );
                    writer.Close( );
                }


                Response.Redirect( "Home/Result?query=" + query );
            }

            return View( );
        }

        public IActionResult Result() {
            var query = Request.Query[ "query" ].ToString( );
            Rank rank = new Rank( );
            var result = rank.BM25( TextNormalizer.Normalization(query) );

            ViewData[ "snippets" ] = result;
            FullPath.INUSE = false;
            return View( );
        }

        public IActionResult Crawler() {
            ViewData[ "Message" ] = "Recupera htmls na web.";
            ViewData[ "grafico" ] = new List<long>( );
            var seeds = Request.Query[ "seeds" ].ToString( );
            var limite = Request.Query[ "limite" ].ToString( );

            Scheduler sch = new Scheduler( );
            sch.Load( );
            var teste = false;
            var seedsCount = 0;
            if (!Request.QueryString.Value.Equals( "" )) {
                if (sch.PendingUrls.Count( ) <= 0) {
                    seedsCount = sch.Initialize( seeds );
                    teste = true;
                }
                sch.PopuleLocalCorpus( teste ? seedsCount: sch.UsedURLS.Count( ), (limite.Equals( "" ) ? 100 : Int32.Parse( limite ) + (teste ? 0 : sch.UsedURLS.Count( ) )) );
                sch.Save( );
                Print.WriteLine( ">>> Salvando resultados em disco" );
                Response.Redirect( "Crawler" );
            }
            sch.Load( );

            ViewData[ "htmls" ] = Directory.GetFiles( FullPath.PATH_COLLECTION_CORPUS ).Count( );
            ViewData[ "tempototal" ] = ConvertTime.Convert( sch.SeedsTime + sch.PopuleTime ) ;
            ViewData[ "urlspend" ] = sch.URLS.Count( );
            ViewData[ "media" ] = sch.CollectionRate.Sum( ) <= 0 ? 1 : sch.CollectionRate.Sum( ) / sch.CollectionRate.Count( ) <= 0 ? 1 : sch.CollectionRate.Count;
            ViewData[ "grafico" ] = sch.CollectionRate;



            return View( );
        }

        public void CleanCollection() {
            if (System.IO.File.Exists( FullPath.PATH_CRAWLER_DATA )) {
                System.IO.File.Delete( FullPath.PATH_CRAWLER_DATA );
            }
            if (System.IO.File.Exists( FullPath.PATH_QUERY )) {
                System.IO.File.Delete( FullPath.PATH_RANK_DOC_LEN );
            }
            if (System.IO.Directory.Exists( FullPath.PATH_COLLECTION_CORPUS )) {
                foreach (var file in Directory.GetFiles( FullPath.PATH_COLLECTION_CORPUS )) {
                    System.IO.File.Delete( file );
                }
            }

            CleanIndex( );
            Response.Redirect( "Crawler" );
        }

        public void ColetarWT10G() {
            WT10G w = new WT10G( );
            w.GenerateCorpus( @"C:\Users\p-lea\Desktop\WT10G\corpus" );
        }

        public IActionResult Indexer() {
            ViewData[ "Message" ] = "Indexa as paginas da coleção";
            InvertedIndex invertedIndex = new InvertedIndex( );
            Vocabulary vocabulary = new Vocabulary( );

            Index cp = new Index( invertedIndex.Load( ), vocabulary.Load( ), 0 );
            if (!Request.QueryString.Value.Equals( "" )) {
                Task.Run( () => cp.ParserAndTokenizer( ) );                
                Response.Redirect( "Indexer" );
            }
            ViewData[ "vocabulario" ] = cp.Vocabulary;
            ViewData[ "indice" ] = cp.InvertedIndex;
            ViewData[ "vocabulario-tamanho" ] = cp.Vocabulary.Tokens.Count( );
            ViewData[ "colecao" ] = cp.InvertedIndex.CollectionSize;
            ViewData[ "tempo" ] = Component.Utils.ConvertTime.Convert( cp.InvertedIndex.Time );
            return View( );
        }

        public void CleanIndex() {
            if (System.IO.File.Exists( FullPath.PATH_INVERTED_INDEX )) {
                System.IO.File.Delete( FullPath.PATH_INVERTED_INDEX );
            }
            if (System.IO.File.Exists( FullPath.PATH_VOCABULARY )) {
                System.IO.File.Delete( FullPath.PATH_VOCABULARY );
            }

            if (System.IO.File.Exists( FullPath.PATH_VOCABULARY )) {
                System.IO.File.Delete( FullPath.PATH_VOCABULARY );
            }

            if (System.IO.File.Exists( FullPath.PATH_TEMPORARY_TRIPLES )) {
                System.IO.File.Delete( FullPath.PATH_TEMPORARY_TRIPLES );
            }
            Response.Redirect( "Indexer" );
        }


        public string[ ] AutoComplete( string text ) {
            if (text == null) {
                return null;
            }
            Vocabulary vocabulary = new Vocabulary( );
            vocabulary.Load( );
            var trie = new Trie<string>( );
            foreach (var token in vocabulary.Tokens) {
                trie.Add( token.Key, token.Key );
            }
            List<string> queryLog = new List<string>( );
            if (System.IO.File.Exists( FullPath.PATH_QUERY_LOG )) {
                using (StreamReader reader = new StreamReader( FullPath.PATH_QUERY_LOG )) {
                    queryLog = JsonConvert.DeserializeObject<List<string>>( reader.ReadToEnd( ) );
                }
            }
            var words = text.Split( ' ' );
            var resp1 = trie.Retrieve( words[ words.Length - 1 ] ).ToArray( );
            var resp = queryLog;
            resp.AddRange( resp1 );
            return resp.ToArray( );
        }

        public IActionResult Error() {
            return View( );
        }
    }
}
