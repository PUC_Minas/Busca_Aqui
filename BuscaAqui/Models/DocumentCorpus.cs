﻿using BuscaAqui.Controllers.Component.Utils;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace BuscaAqui.Models {
    public class DocumentCorpus {
        public int Index { get; set; }
        public string ExternalIndex { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string Url { get; set; }
        public string Source { get; set; }  
        public int SourceIndex { get; set; }

        public static void Save( DocumentCorpus corpus ) {
            using (var writer = new StreamWriter( FullPath.PATH_COLLECTION_CORPUS + corpus.Index + ".json" )) {
                writer.Write( JsonConvert.SerializeObject( corpus, Formatting.Indented ) );
                writer.Close( );
            }
        }

        public static DocumentCorpus Load( int name ) {
            var resp = new DocumentCorpus( );
            if (File.Exists( FullPath.PATH_COLLECTION_CORPUS + @"\" + name + ".json" )) {
                using (StreamReader reader = new StreamReader( FullPath.PATH_COLLECTION_CORPUS + @"\" + name + ".json" )) {
                    var text = reader.ReadToEnd( );
                    resp = JsonConvert.DeserializeObject<DocumentCorpus>( text );
                }
            }
            return resp;
        }
    }
}
