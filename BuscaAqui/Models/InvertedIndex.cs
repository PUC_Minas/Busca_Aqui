﻿using BuscaAqui.Controllers.Component;
using BuscaAqui.Controllers.Component.Utils;
using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace BuscaAqui.Models {
    [Serializable]
    public class InvertedIndexSimple {
        public Dictionary<int, OccurrenceList> Index { get; set; }
        public long Time { get; set; }
        public int CollectionSize { get; set; }


        public InvertedIndexSimple() {
            Index = new Dictionary<int, OccurrenceList>( );
        }
    }

    [Serializable]
    public class InvertedIndex {
        public ConcurrentDictionary<int, OccurrenceList> Index { get; set; }
        public long Time { get; set; }
        public int CollectionSize { get; set; }

        [NonSerialized]
        private Object lockSearch = new Object( );

        [NonSerialized]
        private Object lockInsert = new Object( );

        public InvertedIndex() {
            Index = new ConcurrentDictionary<int, OccurrenceList>( );
        }

        public void Add( int tokenId, OccurrenceList occurrenceList ) {
            lock (lockInsert) {
                OccurrenceList list = new OccurrenceList( );
                Index.TryGetValue( tokenId, out list );
                if (list != null) {
                    list.List.AddRange( occurrenceList.List );
                    list.Idf = Math.Log( ( CollectionSize - list.List.Count( ) + 0.5 / list.List.Count( ) + 0.5 ), 2 );
                    Index[ tokenId ] = list;
                } else {
                    occurrenceList.Idf = Math.Log( ( CollectionSize - occurrenceList.List.Count( ) + 0.5 / occurrenceList.List.Count( ) + 0.5 ), 2 );
                    Index.TryAdd( tokenId, occurrenceList );
                }
            }
        }

        public OccurrenceList Search( int tokenId ) {
            lock (lockSearch) {
                Index.TryGetValue( tokenId, out OccurrenceList occ );
                return occ;
            }
        }

        public void Save() {
            using (StreamWriter s = new StreamWriter( FullPath.PATH_INVERTED_INDEX )) {
                s.Write( JsonConvert.SerializeObject( new InvertedIndexSimple {
                    Index = Index.ToDictionary( ( KeyValuePair<int, OccurrenceList> kvp ) => kvp.Key,
                                                      ( KeyValuePair<int, OccurrenceList> kvp ) => kvp.Value),
                    Time = Time,
                    CollectionSize = CollectionSize
                }, Formatting.Indented ));
                s.Close( );
            }
        }

        public InvertedIndex Load() {
            if (!File.Exists( FullPath.PATH_INVERTED_INDEX )) {
                Index = new ConcurrentDictionary<int, OccurrenceList>( );
                CollectionSize = 0;
                Time = 0;
            } else {
                using (StreamReader s = new StreamReader( FullPath.PATH_INVERTED_INDEX )) {
                    var loaded = s.ReadToEnd( );
                    var index = JsonConvert.DeserializeObject<InvertedIndexSimple>( loaded );
                    CollectionSize = index.CollectionSize;
                    Time = index.Time;
                    Index = new ConcurrentDictionary<int, OccurrenceList>( index.Index );
                    s.Close( );
                }
            }
            return new InvertedIndex { Index = Index, CollectionSize =  CollectionSize, Time = Time};
        }
    }
}
