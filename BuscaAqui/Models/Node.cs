﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BuscaAqui.Models {
    public class Node {
        public int Index { get; set; }
        public string Source { get; set; }
        public Stack<string> Urls { get; set; }
    } 
}
