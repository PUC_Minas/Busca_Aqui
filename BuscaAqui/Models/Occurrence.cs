﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BuscaAqui.Models {
    [Serializable]
    public class Occurrence {
        public int DocumentID { get; set; }
        public List<int> WordPositions { get; set; }

        public Occurrence() {
            WordPositions = new List<int>( );
        }

        public Occurrence( int doc, List<int> pos ) {
            WordPositions = pos;
            DocumentID = doc;
        }
    }
}
