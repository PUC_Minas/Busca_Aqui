﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BuscaAqui.Models {
    [Serializable]
    public class OccurrenceList {
        public List<Occurrence> List { get; set; }
        public double Idf { get; set; }

        public OccurrenceList() {
            List = new List<Occurrence>( );
        }
    }
}
