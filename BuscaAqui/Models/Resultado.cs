﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BuscaAqui.Models {
    public class Resultado {
        public long CrawlerTempo { get; set; }
        public List<long> CrawlerTaxaDeColeta { get; set; } 
        public List<string> CrawlerUrlsPendentes { get; set; }


    }
}
