﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BuscaAqui.Models {
    public class Token {
        public int TokenId { get; set; }
        public string Word { get; set; }

        public Token( string word ) {
            Word = word;
        }
    }
}
