﻿using BuscaAqui.Controllers.Component;
using BuscaAqui.Controllers.Component.Utils;
using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace BuscaAqui.Models {
    public class VocabularySimples {

        public Dictionary<string, int> Tokens;
        public int AVG_DocLen;
        public int Contador;

        public VocabularySimples() {
            Tokens = new Dictionary<string, int>( );
        }
    }

        public class Vocabulary {

        public ConcurrentDictionary<string, int> Tokens;
        public int AVG_DocLen;
        private int Contador;

        [NonSerialized]
        private Object lockSearch = new Object( );

        [NonSerialized]
        private Object lockInsert = new Object( );

        public Vocabulary() {
            Tokens = new ConcurrentDictionary<string, int>( );
            AVG_DocLen = 0;
            Contador = 0;
        }

        public int Insert( string token ) {
            lock (lockInsert) {
                Contador++;
                return Tokens.GetOrAdd( token, Contador );
            }
        }

        public int Search( string word ) {
            lock (lockSearch) {
                Tokens.TryGetValue( word, out int number );
                return number;
            }
        }

        public string Search( int key ) {
            return Tokens.SingleOrDefault( token => token.Value == key ).Key;
        }

        public Vocabulary Load() {
            if (!File.Exists( FullPath.PATH_VOCABULARY )) {
                Tokens = new ConcurrentDictionary<string, int>( );
            } else {
                using (StreamReader reader = new StreamReader( FullPath.PATH_VOCABULARY )) {
                    var text = reader.ReadToEnd( );
                    var vocabulary = JsonConvert.DeserializeObject<VocabularySimples>( text );
                    Tokens = new ConcurrentDictionary<string, int>( vocabulary.Tokens);
                    AVG_DocLen = vocabulary.AVG_DocLen;
                    Contador = vocabulary.Contador;
                    reader.Close( );
                }
            }
            return new Vocabulary { Tokens = Tokens, AVG_DocLen = AVG_DocLen, Contador = Contador };
        }

        public void Save() {
            using (StreamWriter s = new StreamWriter( FullPath.PATH_VOCABULARY )) {
                s.Write( JsonConvert.SerializeObject( 
                    new VocabularySimples {
                        Tokens = Tokens.ToDictionary( ( KeyValuePair<string, int> kvp ) => kvp.Key,
                                                     ( KeyValuePair<string, int> kvp ) => kvp.Value),

                        AVG_DocLen = AVG_DocLen,
                        Contador = Contador
                    } , Formatting.Indented
                ) );
                s.Close( );
            }
        }
    }
}
