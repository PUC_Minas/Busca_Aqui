# Busca aqui - Maquina de busca

**Progresso:** CONCLUÍDO<br />
**Autor:** Paulo Victor de Oliveira Leal<br />
**Data:** 2018/2019<br />

### Objetivo
- Implementar uma máquina de busca completa, dividindo em três partes: crawler, indexer e query processor. A eficiência da máquina de busca é avaliada.

### Observação
IDE:  [Visual Studio 2017](https://visualstudio.microsoft.com)<br />
Linguagem: [C#](https://docs.microsoft.com/pt-br/dotnet/core/)<br />
Banco de dados: Não utiliza<br />

### Execução

    $ dotnet restore
    $ dotnet run --hosturl http://IP+PORTA

### Teste
- Para testar:
- Abra o navegador a url informada durante a compilação: 
    - http://IP+PORTA/
- Na tela inicial ou clicando no menu Home	
	- Entre com a query que deseja procurar
- Na barra de menus acima, clique em "Coletor"
    - Entre com os seeds desejados
        - Obs: Um por linha e url completa "http:\\..."
    	- Obs2: Um arquivo com seeds utilizado encontra no zip
    - Entre com a quantidade de HTMLs de limite desejada
    - Clique em iniciar
        - Obs: Caso a página mostre erro, é só o time out que expirou será recarregado automaticamente
    - As estatisticas serão exibidas ao terminar a coleta
    - O corpus pode ser encontrado no caminho do projeto dentro da pasta "bin/dotnet core 2.0/Corpus"
- Na barra de menus acima, clique em "Indxaor"
    - Clique no botão "Iniciar", para que o sistema gere o índice
        -  Obs: 1% do índice será exibido na tabela ao lado dos botões
		-  Obs2: A medida que o sistema vai indexando, a coleção vai sendo comprimida

### Seeds
	- https://www.nytimes.com/
	- http://www.otempo.com.br/super-noticia/
	- https://www.em.com.br/
	- https://pt.wikipedia.org/
	- http://www.otempo.com.br/
	- http://www.estadao.com.br/
	- http://www.folha.uol.com.br/ 
	- https://noticias.uol.com.br/jornais/
		
### Objetivos futuros		
	- Inserir quantiidade de indexados
	- Serializar em binario
	- Acesso aleatorio
	- No coletor colocar outra curva para taxa de escrita		
	- Indexar a coleção do [Trec]
		
### Pacotes

| Nome | Função |
| ------ | ------ |
| [HtmlAgilityPack] | Download e parse do html |
| [RobotsTXT] | Parse do arquivo Robots.txt |

[HtmlAgilityPack]: <https://www.nuget.org/packages/HtmlAgilityPack/>
[RobotsTXT]: <https://www.nuget.org/packages/Com.Bekijkhet.RobotsTxt/>
[Trec]: <http://trec.nist.gov/data/t10.web.html/>

### Contribuição

Esse projeto está completo e livre para o uso.

## Licença
<!---

[//]: <> (The Laravel framework is open-sourced software licensed under the [MIT license]https://opensource.org/licenses/MIT)

-->